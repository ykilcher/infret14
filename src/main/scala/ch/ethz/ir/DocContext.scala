package ch.ethz.ir

/**
 * Created by yk on 21.10.14.
 */
case class DocContext(tfidf: Map[String,Double],tf: Map[String,Int]){
  val tfidfLength = if(tfidf != null) utilIR.vlength(tfidf) else 0
  val tfsum = if(tf != null) tf.values.sum else 0
}
