package ch.ethz.dal.tinyir.processing

object Tokenizer {
  def tokenize (text: String) : List[String] =
//    text.split("[ .,;:?!\t\n\r\f]+").map(w => w.replaceAll("\\W","").filterNot(w => w.equals(""))).toList
  text.split("[ .,;:?!\t\n\r\f]+").toList
}