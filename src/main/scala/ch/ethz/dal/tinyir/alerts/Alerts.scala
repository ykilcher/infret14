package ch.ethz.dal.tinyir.alerts

import ch.ethz.dal.tinyir.processing.Document
import ch.ethz.ir.DocContext

import scala.collection.mutable.PriorityQueue
import scala.math.Ordering.Implicits._

case class ScoredResult (title : String, score: Double)

class Alerts (q: Query, n: Int) {

  val query = q

  
  // score a document and try to add to results
  def process(doc: Document) : Boolean = {
    val score = query.score(doc)
    add(ScoredResult(doc.ID,score))
  }
  
  // get top n results (or m<n, if not enough docs processed)
  def results = heap.toList.sortBy(res => -res.score)    

    // heap and operations on heap
  private val heap = new PriorityQueue[ScoredResult]()(Ordering.by(score))
  private def score (res: ScoredResult) = -res.score 
  private def add(res: ScoredResult) : Boolean = {    
    if (heap.size < n)  { // heap not full
      heap += res
      true
    } else if (heap.head.score < res.score) {
        heap.dequeue
        heap += res
        true
      } else false
  }
  

}

