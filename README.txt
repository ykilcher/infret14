the main function is in is ch.ethz.ir.hw

the (ill-named) variable "languageModel" can be set to 1 for tfIdf or 2 for the language model

for the tfIdf I've used the formula on page 13 of lecture 4, but without the first log (don't know why it works better this way)

for the language model I've used the formula on page 8 of lecture 5, but left the constants away I didn't have time to fine tune lambda

with both models, I get MAPs of ~15%

the output in the end consists of the stopwatch time, the performance reports for the individual queries and the MAP. if you would like to see the results for the queries, there is a variable "outputResults" towards the bottom of the code

if you decide to add query 101 and so on, change the "maxQuery" variable appropriately. if you don't do this, your query will still be processed, but not output (I'm sorry, I know this is lame)

the system can process the dataset in about 20 minutes if stemming is commented out. with stemming it takes around 1 hour and 20 minutes

if you don't have so much time, change the "n" variable to something small to process only a fraction of the data
