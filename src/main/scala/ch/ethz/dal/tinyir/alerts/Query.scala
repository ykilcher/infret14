package ch.ethz.dal.tinyir.alerts

import ch.ethz.dal.tinyir.processing.{Document, Tokenizer}
import ch.ethz.ir.{DocContext, utilIR}
import com.github.aztek.porterstemmer.PorterStemmer

class Query (query: String) {
  val qterms = Tokenizer.tokenize(query).filterNot(t => utilIR.stopWords.contains(t)).map(PorterStemmer.stem).toList
//  val qterms = Tokenizer.tokenize(query).filterNot(t => utilIR.stopWords.contains(t))
//  val qterms = Tokenizer.tokenize(query)
  val length = qterms.length

  def score (doc: Document) : Double = {
//    val tfs : Map[String,Int]= doc.tokens.groupBy(identity).mapValues(l => l.length)
    val qtfs = qterms.flatMap(q => doc.tf.get(q))
    val numTermsInCommon = qtfs.length 
    val docLen = doc.tf.values.map(x => x*x).sum.toDouble  // Euclidian norm
    val queryLen = qterms.length .toDouble  
    val termOverlap = qtfs.sum.toDouble / (docLen * queryLen)
    
    // top ordering is by terms in common (and semantics)
    // integer range from 0...qterms.length
    // on top of this a tf-based overlap score in range [0;1[ is added
    numTermsInCommon + termOverlap
  }
}