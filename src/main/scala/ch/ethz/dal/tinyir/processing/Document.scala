package ch.ethz.dal.tinyir.processing

import com.github.aztek.porterstemmer.PorterStemmer
import ch.ethz.ir.utilIR

import util.Try

abstract class Document {
  def title  : String 
  def body   : String
  def name   : String 
  //def ID     : Int = Try(name.toInt).getOrElse(-1)
  def ID     : String = name
  def date   : String
  def codes  : Set[String] = Set()   
  def content: String  
  def tokens : List[String] = Tokenizer.tokenize(content).filterNot(t=>utilIR.stopWords.contains(t)).map(PorterStemmer.stem)
//  def tokens : List[String] = Tokenizer.tokenize(content).filterNot(t => utilIR.stopWords.contains(t))
//  def tokens : List[String] = Tokenizer.tokenize(content)
  var tf : Map[String,Int] = null
  var tfsum = 0
//  var tfidfLength = if(tfidf != null) utilIR.vlength(tfidf) else 0
//  var tfsum = if(tf != null) tf.values.sum else 0
}
