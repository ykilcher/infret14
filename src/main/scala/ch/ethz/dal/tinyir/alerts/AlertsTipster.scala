package ch.ethz.dal.tinyir.alerts

import ch.ethz.dal.tinyir.io.TipsterStream
import ch.ethz.dal.tinyir.processing.Document
import ch.ethz.dal.tinyir.util.StopWatch
import ch.ethz.dal.tinyir.lectures.TipsterGroundTruth
import ch.ethz.dal.tinyir.lectures.PrecisionRecall
import ch.ethz.ir.{utilIR, DocContext}

class AlertsTipster(q: Query, n: Int) extends Alerts(q,n)

object AlertsTipster {


  def main(args: Array[String]) {
    val folder = "/usr/yk/data/tipster/"
    val query = "Airbus Subsidies"
    val num = 100
    val alerts = new AlertsTipster(new Query(query) {
      override def score(doc: Document): Double = {
        val tfs: Map[String, Int] = doc.tokens.groupBy(identity).mapValues(l => l.length)
        val qtfs = qterms.flatMap(q => tfs.get(q))
        val numTermsInCommon = qtfs.length
        val docLen = tfs.values.map(x => x * x).sum.toDouble // Euclidian norm
        val queryLen = qterms.length.toDouble
        val termOverlap = qtfs.sum.toDouble / (docLen * queryLen)

        // top ordering is by terms in common (and semantics)
        // integer range from 0...qterms.length
        // on top of this a tf-based overlap score in range [0;1[ is added
        numTermsInCommon + termOverlap
      }
    },num)
    val tipster = new TipsterStream(folder + "zips")
    
    val sw = new StopWatch; sw.start
    var iter = 0
    for (doc <- tipster.stream.take(20000)) {
      iter += 1

      val context: DocContext =
        new DocContext(
          null,
          null
        )
      alerts.process(doc)
      if (iter % 20000 ==0) {
        println("Iteration = " + iter)
        alerts.results.foreach(println)    
      }  
    }
    sw.stop
    println("Stopped time = " + sw.stopped)
    alerts.results.foreach(println)  
    val rel = new TipsterGroundTruth(folder + "qrels").judgements.get("51").get.toSet
    val ret = alerts.results.map(r => r.title)
    val pr = new PrecisionRecall(ret,rel)
    println(pr.relevIdx.mkString(" "))
    println(pr.precs.mkString(" "))
    println(pr.iprecs.mkString(" "))
  }

}