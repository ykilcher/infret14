package ch.ethz.ir

import ch.ethz.dal.tinyir.alerts.{Query, AlertsTipster}
import ch.ethz.dal.tinyir.io.TipsterStream
import ch.ethz.dal.tinyir.lectures.{PrecisionRecall, TipsterGroundTruth}
import ch.ethz.dal.tinyir.processing.{XMLDocument, Document}
import ch.ethz.dal.tinyir.util.StopWatch
import ch.ethz.ir.utilIR._

import scala.io.Source
import scala.math

/**
 * Created by yk on 15.10.14.
 */
object hw {

  val languageModel = 1 //1 for tfidf, 2 for lm


  def main(args: Array[String]) {
    val folder = "/usr/yk/data/tipster/"
    val num = 100
    val tipster = new TipsterStream(folder + "zips")

    val sw = new StopWatch; sw.start
    val n = tipster.stream.length
//    val n = 1000
    var df = collection.mutable.Map[String,Int]()
    val cf = collection.mutable.Map[String,Int]()

    if(languageModel == 1) {
      for (doc <- tipster.stream.take(n)) {
        df ++= doc.tokens.distinct
          .map(t => t -> (1 + df.getOrElse(t, 0)))
      }
    } else {
      for (doc <- tipster.stream.take(n)) {
          cf ++= tf(doc.tokens).map(t => t._1 -> (t._2 + cf.getOrElse(t._1,0)))
      }
    }

    println("pass 1 done")

    val lidf = utilIR.lidf(df.toMap,n)
    df = null
    val cfsum = cf.values.sum

    def tfIdf(terms: List[String]) =
      utilIR.logtf(terms).map(f => f._1 -> (1.0 + f._2) * lidf.getOrElse(f._1,0.0))

    val lambda = 0.1


//    class TfidfVectorQuery(q: String) extends Query(q) {
//      lazy val qtfidf = tfIdf(qterms)
//      lazy val qtfidfLength = vlength(qtfidf)
//
//      override def score(doc: Document): Double = {
//        if (qtfidf.isEmpty)
//          return 0.0
//        dot(qtfidf, doc.tfidf) / (qtfidfLength * doc.tfidfLength)
//      }
//    }

    class TfidfQuery(q: String) extends Query(q){
      override def score(doc: Document): Double = {
        qterms.filter(q => doc.tf.contains(q)).map(q => q -> (doc.tf.get(q).get))
          .map(f => (1.0 + f._2.toDouble/ doc.tfsum) * lidf.getOrElse(f._1,0.0)).sum
      }

      /*
      override def score(doc: Document): Double = {
        val qtfs = qterms.flatMap(q => doc.tf.get(q))
        val numTermsInCommon = qtfs.length
        val docLen = doc.tf.values.map(x => x * x).sum.toDouble // Euclidian norm
        val queryLen = qterms.length.toDouble
        val termOverlap = qtfs.sum.toDouble / (docLen * queryLen)

        // top ordering is by terms in common (and semantics)
        // integer range from 0...qterms.length
        // on top of this a tf-based overlap score in range [0;1[ is added
        numTermsInCommon + termOverlap
      }
      */
    }

    class LanguageModelQuery(q: String) extends Query(q){
      override def score(doc: Document): Double = {
          this.qterms.distinct.filter(qt => doc.tf.contains(qt)).map(
            w => log2((doc.tf.get(w).get.toDouble / doc.tfsum) / (cf.get(w).get.toDouble / cfsum) * (1.0 - lambda) / lambda + 1.0)
          ).sum //+ log2(lambda)
      }

    }


    val topicString = Source.fromFile(folder + "topics").getLines().mkString(" ")
//    val numPattern = """<num>\s+Number:\s+(\d+)""".r
//    val nums = numPattern.findAllIn(topicString).matchData.map(m => m.group(1)).toList
    val titlePattern = """<title>\s+Topic:\s+(.+?)\s+<desc>""".r
    val titles = titlePattern.findAllIn(topicString).matchData.map(m => m.group(1)).map(t => t.replaceAll("""\s+"""," ").trim()).toList
//    val alerts = titles.map( t => new AlertsTipster(new TfidfQuery(t),num) )
//    val alerts = titles.map( t => new AlertsTipster(new Query(t),num) )
      val alerts = titles.map(t => new AlertsTipster(
        if (languageModel == 1)
          new TfidfQuery(t)
        else
          new LanguageModelQuery(t)
      ,num))

    println("starting pass 2")

    for (doc <- tipster.stream.take(n)) {
      doc.tf = utilIR.tf(doc.tokens)
      doc.tfsum = doc.tf.values.sum

      alerts.foreach(_.process(doc))
      doc.tf = null
    }

    sw.stop
    println("Stopped time = " + sw.stopped)
    val groundTruth = new TipsterGroundTruth(folder + "qrels")

    val haveJudgements = (51 to 90).toList

    val outputResults = false

    val APs = for(an <- alerts.zip(haveJudgements)) yield {
      val rel = groundTruth.judgements.get(an._2.toInt.toString).get.toSet
      val ret = an._1.results.map(r => r.title)
      val pr = new PrecisionRecall(ret, rel)
      println("Query " + an._2 + " AP: " + pr.avgIPrec)
      if(outputResults) {
        println("results:")
        an._1.results.take(num).foreach(println)
      }
      pr.avgIPrec
    }

    val MAP = APs.sum/haveJudgements.length

    println("MAP: " + MAP)

    val maxQuery = 100

    val doNotHaveJudgements = (91 to maxQuery).toList
    if(outputResults) {
      for (j <- doNotHaveJudgements) {
        println("Unjudged Query " + j + " results:")
        alerts(j - 51).results.take(num).foreach(println)
      }
    }
  }
}

